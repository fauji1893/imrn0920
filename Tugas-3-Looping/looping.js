// soal 1 Looping while
var iterasi = 0;
console.log("Soal 1");
console.log("LOOPING PERTAMA");
while(iterasi<20){
    iterasi+=2;
    console.log(iterasi + " - I love coding");
}
console.log("LOOPING KEDUA");
while(iterasi>0){
    console.log(iterasi + " - I will become a mobile developer");
    iterasi-=2;
}

// soal 2 Looping for
console.log("\nSoal 2");
for(var i = 1; i<21;i++){
    if(i%2==1){
        if(i%3==0){
            console.log(i + " - I Love Coding");
        }
        else{
            console.log(i + " - Santai");
        }
    }
    else{
        console.log(i + " - Berkualitas");
    }
    
}

// soal 3 Membuat persegi panjang
console.log("\nSoal 3");
for(var i = 0; i<4; i++){
    console.log("########");
}

// soal 4 Membuat tangga
console.log("\nSoal 4");
var str = "";

for(var i = 0; i<7; i++){
    str += "#";
    console.log(str);
}

// soal 5 Membuat papan catur
console.log("\nSoal 5");

for(var i = 0; i<8; i++){
    if(i%2==0)
        console.log(" # # # #");
    else
        console.log("# # # # ");
}
