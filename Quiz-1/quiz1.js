function palindrome(inputString){
    var stringReverse = '';
    for(var i = inputString.length-1; i>=0; i--){
        stringReverse += inputString[i];
    }
    if(inputString==stringReverse){
        return true;   
    }
    else{
        return false;
    }
    
}
console.log(palindrome("kasur rusak")) // true 
console.log(palindrome("haji ijah")) // true 
console.log(palindrome("nabasan")) // false 
console.log(palindrome("nababan")) // true 
console.log(palindrome("jakarta")) // false

function ularTangga(){
    for(var x = 10; x>0;x--){
        var result = '';
        if(x%2==0){
            for(var y = x*10; y>(x*10-10);y--){
                result += y + ' ';
            }
        }
        else{
            for(var y = (x-1)*10+1; y<=x*10;y++){
                result += y + ' ';
            }
        }
        console.log(result);
    }
}

ularTangga();


function balikString(stringInput){
    var result = '';
    for(var i = stringInput.length-1; i>=0; i--){
        result += stringInput[i];
    }
    return result;
}

console.log(balikString("abcde")) // edcba 
console.log(balikString("rusak")) // kasur 
console.log(balikString("racecar")) // racecar 
console.log(balikString("haji")) // ijah




function DescendingTen(inputNumber){
    var result = '';
    if(inputNumber!=null){
        for(var i = inputNumber; i>inputNumber-10;i--){
            result += i + ' ';
        }
        return result;
    }
    else return -1;
}

console.log(DescendingTen(100)); // 100 99 98 97 96 95 94 93 92 91 
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1 
console.log(DescendingTen()) // -1