// Soal 1 Range
function range(start, end){
    var arrayRange = [];
    if(start<end){
        for(var i = start; i<=end; i++){
            arrayRange.push(i);
        }
    }
    else{
        for(var i = start; i>=end; i--){
            arrayRange.push(i);
        }
    }

    return arrayRange;
}

console.log("No.1");
console.log(range(0,20));


// Soal 2 Range with Step
function rangeWithStep(start, end, step){
    var arrayRange = [];
    if(start<end){
        for(var i = start; i<=end; i+=step){
            arrayRange.push(i);
        }
    }
    else{
        for(var i = start; i>=end; i-=step){
            arrayRange.push(i);
        }
    }

    return arrayRange;
}

console.log("\nNo.2");
console.log(rangeWithStep(20,0,3));


// Soal Sum of Range
function sum(start, end, step = 1){
    var result = 0;
    var arrayRange = rangeWithStep(start,end,step);
    for(var i =0; i<arrayRange.length;i++){
        result += arrayRange[i];
    }
    return result;
}
console.log("\nNo.3");
console.log(sum(1,10,5));


// Soal 4 Array Multidimensi
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

function dataHandling(inputArray){
    for(var i = 0; i<inputArray.length;i++){
        console.log('Nomor ID = '+inputArray[i][0]);
        console.log('Nama = '+inputArray[i][1]);
        console.log('TTL = '+inputArray[i][2]);
        console.log('Hobi = '+inputArray[i][3]);
        console.log('');
    }
}
console.log("\nNo.4");
dataHandling(input);

// Soal 5 Balik Kata
function balikKata(stringInput){
    var result = '';
    for(var i = stringInput.length-1; i>=0; i--){
        result += stringInput[i];
    }
    return result;
}
console.log("\nNo.5");
console.log(balikKata('Cinta'));

// Soal 6 Methde Array
var input2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];

function dataHandling2(inputArray){
    var bulan = '';
    var tanggalLahirJoin = '';
    inputArray.splice(1,2,"Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
    inputArray.splice(4,1,"Pria","SMA Internasional Metro");
    var tanggalLahir = inputArray[3].split('/');
    switch(tanggalLahir[1]){
        case '01':
            bulan = 'Januari';
            break;
        case '02':
            bulan = 'Februari';
            break;
        case '03':
            bulan = 'Maret';
            break;            
        case '04':
            bulan = 'April';
            break;            
        case '05':
            bulan = 'Mei';
            break;            
        case '06':
            bulan = 'Juni';
            break;            
        case '07':
            bulan = 'Juli';
            break;            
        case '08': 
            bulan = 'Agustus';
            break;                   
        case '09':        
            bulan = 'September';
            break;        
        case '10':
            bulan = 'Oktober';
            break;                    
        case '11':
            bulan = 'November';
            break;                    
        case '12':
            bulan = 'Desember';
            break;                    

    }
    tanggalLahirJoin = tanggalLahir.join('-');
    console.log(inputArray);
    console.log(bulan);
    console.log(tanggalLahir.sort(function(a, b){return b-a}));
    console.log(tanggalLahirJoin);
    console.log(inputArray[1].slice(0,15));

    
}
console.log("\nNo.6");
dataHandling2(input2);

