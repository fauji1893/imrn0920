// Soal 1
function teriak(){
    return "Halo Sanbers!";
}


console.log("Soal 1");
console.log(teriak());

// Soal 2
function kalikan(a,b){
    return a*b;
}

var num1 = 5;
var num2 = 7;

var hasilKali = kalikan(num1,num2);
console.log("\nSoal 2");
console.log(num1 +" * "+ num2 + " = " +  hasilKali);

// Soal 3
function introduce(nama, umur, alamat, hobi){
    var hasil = "Nama saya " + nama +", umur saya "+umur+" tahun, alamat saya di "+alamat+", dan saya punya hobby yaitu "+hobi+"!";

    return hasil
}

var name = "James"
var age = 1
var address = "Jln. Jalan, Depok"
var hobby = "Ngemil"
 
var perkenalan = introduce(name, age, address, hobby);
console.log("\nSoal 3");
console.log(perkenalan);

