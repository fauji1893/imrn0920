var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
// Tulis code untuk memanggil function readBooks di sini
bacaBuku = (durasi, indexBaca) =>{
    if(books[indexBaca]!=null){
        readBooksPromise(durasi, books[indexBaca++])
            .then(function (fulfilled){
                bacaBuku(fulfilled,indexBaca);
            })
            .catch(function(error){})
        
    }
}

bacaBuku(10000,0);
