var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
bacaBuku = (durasi, indexBaca) =>{
    if(books[indexBaca]!=null){
        readBooks(durasi, books[indexBaca++], (sisaWaktu)=>{
            bacaBuku(sisaWaktu,indexBaca)
        })
    }
}

bacaBuku(10000,0);
