// soal 1 Membuat kalimat
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

var sentence1 = "";

sentence1 = word + ' ' + second + ' ' + third+' '+fourth+' '+fifth+' '+sixth+' '+seventh;
console.log(sentence1);


// soal 2 Mengurai kalimat (Akses karakter dalam string)
var sentence = "I am going to be React Native Developer";

var res = sentence.split(' ');
var firstWord = res[0]; 
var secondWord = res[1];; 
var thirdWord = res[2]; 
var fourthWord = res[3]; 
var fifthWord = res[4]; 
var sixthWord = res[5]; 
var seventhWord = res[6]; 
var eighthWord = res[7]; 

console.log('First Word: ' + firstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord)


// soal 3 Mengurai Kalimat (Substring)
var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14); 
var thirdWord2 = sentence2.substring(15, 17);
var fourthWord2 = sentence2.substring(18, 20); 
var fifthWord2 = sentence2.substring(21, 25); 

console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

// soal 4 Mengurai Kalimat dan Menentukan Panjang String
var sentence3 = 'wow JavaScript is so cool'; 

var res2 = sentence3.split(' ');
var exampleFirstWord3 = res2[0]; 
var secondWord3 = res2[1]; 
var thirdWord3 = res2[2]; 
var fourthWord3 = res2[3]; 
var fifthWord3 = res2[4]; 

var firstWordLength = exampleFirstWord3.length;
var secondWord3Length = secondWord3.length;
var thirdWord3Length = thirdWord3.length;
var fourthWord3Length = fourthWord3.length;
var fifthWord3Length = fifthWord3.length;

console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWord3Length); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWord3Length); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWord3Length); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWord3Length); 
