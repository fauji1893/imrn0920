//soal 1
var nama = "Fauji";
var peran = "Werewolf";

if(nama === ""){
	console.log("nama harus diisi!")
}
else{
	if(peran === ""){
		console.log("Halo "+nama+", pilih peranmu untuk memulai game")
	}
	else{
		console.log("Selamat datang di dunia Werewolf "+nama);
		if(peran === "Penyihir"){
			console.log("Halo Penyihir "+nama+", kamu dapat melihat siapa yang menjadi werewolf!");
		}
		else if(peran === "Guard"){
			console.log("Halo Guard "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf");
		}
		else if(peran === "Werewolf"){
			console.log("Halo Werewolf "+nama+", kamu akan memakan mangsa setiap malam!");
		}
	}
}


//soal 2
var tanggal = 1; //masukkan tanggal dengan nilai antara 1 sampai 31
var bulan = 12 //masukkan bulan dengan nilai antara 1 sampai 12
var tahun = 2000; //masukkan tahun dengan nilai 1900 sampai 2200

var strBulan = "" //untuk menyimpan string Bulan

switch (bulan) {
  case 1:
    strBulan = "Januari";
    break;
  case 2:
    strBulan = "Februari";
    break;
  case 3:
     strBulan = "Maret";
    break;
  case 4:
    strBulan = "April";
    break;
  case 5:
    strBulan = "Mei";
    break;
  case 6:
    strBulan = "Juni";
    break;
  case 7:
    strBulan = "Juli";
    break;
  case 8:
    strBulan = "Agustus";
    break;
  case 9:
    strBulan = "September";
    break;
  case 10:
    strBulan = "Oktober";
    break;
  case 11:
    strBulan = "November";
    break;
  case 12:
    strBulan = "Desember";
    break;
}

console.log(tanggal +" "+strBulan+" "+tahun);