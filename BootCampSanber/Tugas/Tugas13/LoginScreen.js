import React from 'react'
import { Platform, StyleSheet, ScrollView, Text, View, TextInput, Button, TouchableOpacity, KeyboardAvoidingView } from 'react-native'

const LoginScreen = () => {
    return (
        <KeyboardAvoidingView
            behavior={Platform.OS == "android" ? "padding" : "height"}
            style={styles.container}
        >
            <ScrollView>

                <View style={styles.containerView}>
                    <Text style={styles.loginText}>Login</Text>
                    <View style={styles.formatInput}>
                        <Text style = {styles.formText}>Username</Text>
                        <TextInput style = {styles.input}/>
                    </View>
                    <View style={styles.formatInput}>
                        <Text style = {styles.formText}>Password</Text>
                        <TextInput style = {styles.input} secureTextEntry={true}/>
                    </View>
                    <View style={styles.kotakLogin}>
                        <TouchableOpacity style={styles.btLogin}>
                            <Text style={styles.textBt}>Masuk</Text>
                        </TouchableOpacity>
                        <Text style={styles.autoText}>atau</Text>
                        <TouchableOpacity style={styles.btReg}>
                            <Text style={styles.textBt}>Daftar</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </KeyboardAvoidingView>
    )
}

export default LoginScreen

const styles = StyleSheet.create({
    container: {
       flex:1
    },
    loginText:{
        fontSize:30,
        marginTop:30,
        textAlign:"center",
        color:'#003366',
        marginVertical: 20
    },
    formatInput:{
        marginHorizontal: 50,
        marginVertical:5,
        alignContent:'center',
        width:294
    },
    formText:{
        color:'#003366'
    },
    autoText:{
        color: '#3EC6FF',
        fontSize: 20,
        textAlign: 'center'
    },
    input:{
        height:40,
        borderColor:'#003366',
        padding:10,
        borderWidth:1
    },
    btLogin:{
        alignItems: 'center',
        padding:10,
        backgroundColor: '#003366',
        fontSize: 20,
        borderRadius:15,
        marginHorizontal: 150

    },
    btReg:{
        alignItems: 'center',
        padding: 10,
        backgroundColor: '#004466',
        fontSize: 20,
        borderRadius:15,
        marginHorizontal: 150
    }

})
