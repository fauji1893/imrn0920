// Soal 1
var input = [["Abduh", "Muhamad", "male", 1992], ["Ahmad", "Taufik", "male", 1985]]
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)

function arrayToObject(inputArray){
    var personObject = {}
    for(var i = 0; i<inputArray.length;i++){
        personObject.firstName = inputArray[i][0]
        personObject.lastName = inputArray[i][1]
        personObject.gender = inputArray[i][2]
        if(inputArray[i][3]==null||inputArray[i][3]>thisYear){
            personObject.age = 'Invalid Birth Year'
        }
        else{
            personObject.age = thisYear - inputArray[i][3]
        }
        console.log(i+1 +'. '+ personObject.firstName+' '+ personObject.lastName+' : ')
        console.log(personObject)
    }
    
}
arrayToObject(input)
arrayToObject(people)
arrayToObject(people2)

// Soal 2


var daftarHarga = [1500000,500000,250000,175000,50000]
var daftarBarang = {
    "Sepatu Stacattu" : 1500000,
    "Baju Zoro" : 500000,
    "Baju H&N" : 250000,
    "Sweater Uniklooh" : 175000,
    "Casing Handphone" : 50000
}
function shoppingTime(memberId, money){
    var shoppingObject = {}
    var listPurchased = []
    var changeMoney = 0;
    if(memberId==null){
        console.log('Mohon maaf, toko X hanya berlaku untuk member saja')
    }

    else{
        if(money<50000){
            console.log('Mohon maaf, uang tidak cukup')
        }
        else{
            shoppingObject.memberId = memberId
            shoppingObject.money = money
        
            shoppingObject.listPurchased = listPurchased
            shoppingObject.changeMoney = changeMoney

            return shoppingObject
        }
    }
}
// Nomor 2 belum selesai. belum ketemu logikanya. :D
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));


// Soal 3
function naikAngkot(listPenumpang){
    
    var result = []
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    
    for(var i = 0; i<listPenumpang.length;i++){
        var penumpangObject = {}
        penumpangObject.penumpang = listPenumpang[i][0]
        penumpangObject.naikDari = listPenumpang[i][1]
        penumpangObject.tujuan = listPenumpang[i][2]
        penumpangObject.bayar = 2000*(rute.indexOf(listPenumpang[i][2]) - rute.indexOf(listPenumpang[i][1]))
        result.push(penumpangObject)
    
    }

    return result

}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));